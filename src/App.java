import java.util.Scanner;

public class App {

    enum Tipe {
        SAMA_KAKI,
        SAMA_SISI,
        IRREGULAR,
        BUKAN_SEGITIGA
    }

    public static Tipe triangleType(int a, int b, int c) {

        if(a >= (b + c) || b >= (a + c) || c >= (b + a)) {
            return Tipe.BUKAN_SEGITIGA;
        } else if(a == b && b == c) {
            return Tipe.SAMA_SISI;
        } else if(a !=b && b != c && c != a) {
            return Tipe.IRREGULAR;
        } else if ((a == b && b != c ) || (b == c && c != a) || (a != b && c == a) ) {
            return Tipe.SAMA_KAKI;
        }
        
        return null;
    }

    public static void main(String [] args) {

        Scanner in = new Scanner(System.in);
        System.out.println("Masukkan sisi segitiga:");
        int a = in.nextInt();
        int b = in.nextInt();
        int c = in.nextInt();
        
        System.out.println(triangleType(a, b, c));
    }
}
